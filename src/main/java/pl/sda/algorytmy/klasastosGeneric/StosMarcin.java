package pl.sda.algorytmy.klasastosGeneric;

import pl.sda.algorytmy.klasastos.MojError;

public class StosMarcin<T> {
    private int maxSize;
    private Object[] array;
    private int top;

    public StosMarcin(int maxsize) {
        this.maxSize = maxsize;
        this.array = new Object[maxSize];
        this.top = -1;
    }

    public void push(Object o) {
        if (this.top < this.array.length - 1) {
            this.array[++this.top] = o;
        } else {

            Object[] biggerTab = new Object[this.array.length * 2];
            for (int i = 0; i < this.array.length; i++) {
                biggerTab[i] = this.array[i];
            }
            biggerTab[++this.top] = o;

            this.maxSize=2*this.maxSize;
            this.array=biggerTab;

        }
    }


    public Object pop() throws MojError {
        if (top < 0) {
            throw new MojError("Stos nie zawiera wartości do zwrotu");
        }
        return this.array[top--];
    }

    public Object peek() throws MojError {
        if (top == -1) {
            throw new MojError("Stos nie zawiera wartości do wyświetlenia");
        }
        return this.array[top];
    }

    public boolean isEmpty() throws MojError {
        if (this.top < 0) {
            return true;
        } else {
            throw new MojError("Coś tu jest!");
        }
    }

    public int size() {
        return maxSize;
    }

    public boolean contains(Object o) throws MojError {
        if (top > -1) {
            for (int i = 0; i <= top; i++) {
                if (this.array[i].equals(o)) {
                    return true;
                }
            }
        }
        throw new MojError("Szukanego elementu nie znaleziono");
    }

    public void clear() {
        for (int i = 0; i < maxSize; i++) {
            this.array[i] = null;
        }


    }


}
