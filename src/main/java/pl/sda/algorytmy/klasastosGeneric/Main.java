package pl.sda.algorytmy.klasastosGeneric;

import pl.sda.algorytmy.klasastos.MojError;
import pl.sda.algorytmy.klasastos.MojStos;

public class Main {
    public static void main(String[] args) {

        StosMarcin<String> stosMarcin = new StosMarcin<String>(1);


        try {
            System.out.println(stosMarcin.peek());

        } catch (MojError error) {
            System.out.println(error.getMessage());
        }




        try {
            stosMarcin.push("Janko Muzykant");
            System.out.println(stosMarcin.size());
            System.out.println(stosMarcin.peek());
            stosMarcin.push("Janko Muzykant Cyborg");
            stosMarcin.push("Janko po prostu");
            stosMarcin.push("Janko Muzykant Cyborg");
            stosMarcin.push("Janko po prostu");
            System.out.println(stosMarcin.peek());
            System.out.println(stosMarcin.size());

        } catch (MojError error) {
            System.out.println(error.getMessage());
        }


    }
}
