package pl.sda.algorytmy.sortowanieprzezwybieranie;

public class Main {
    public static void main(String[] args) {
        int[] tab = new int[]{2,3,4,5,6,7,-355,2,5,677,5,6,7,4,3,3,5,6,-7};
        wypisz(tab);
        sortuj(tab);
        wypisz(tab);
    }

    public static void wypisz(int[] tab){
        for (int i = 0; i <tab.length ; i++) {
            System.out.print(tab[i]+", ");

        }
        System.out.println("\n");
    }

    public static void sortuj(int[] tab){
        int tmp;
        int ind_najmn;
        for (int i = 0; i <tab.length ; i++) {
            ind_najmn=i;
            for (int j = i+1; j <tab.length ; j++) {
                if (tab[ind_najmn]>tab[j]){
                    ind_najmn=j;
                }
            }
            tmp=tab[ind_najmn];
            tab[ind_najmn]=tab[i];
            tab[i]=tmp;
        }




    }
}
