package pl.sda.algorytmy.klasastos;

public class MojError extends RuntimeException {
    public MojError(String message){
        super(message);
    }
}
