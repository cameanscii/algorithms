package pl.sda.algorytmy.klasastos;

public class Main {

    public static void main(String[] args) {
        MojStos stos = new MojStos(3);
        System.out.println(stos.maxRozmiar);
        System.out.println(stos.peek());
        stos.push("Jacek");
        System.out.println(stos.peek());
        stos.push("Placek");
        stos.push("Placek");
        stos.push("Placek");
        stos.push("Placek");
        stos.push("Placek");
        System.out.println(stos.maxRozmiar);
        System.out.println(stos.isEmpty());
        System.out.println(stos.peek());
        System.out.println(stos.isEmpty());
        System.out.println(stos.pop());
        System.out.println(stos.isEmpty());
        System.out.println(stos.peek());
    }
}
