package pl.sda.algorytmy.wyszukiwanie;

public class Main {
    public static void main(String[] args) {
        int[] tab = new int[]{2, 3, 4, 5, 6, 7, -355, 2, 5, 677, 5, 6, 7, 4, 3, 3, 5, 6, -7};

        System.out.println(szukajRecurencyjnie(-355,0,tab));
    }

    public static int wyszukiwanieliniowe(int x, int[] tab) {
        for (int i = 0; i <tab.length ; i++) {
            if (tab[i]==x){
                return i;
            }

        }
        return -1;
    }

public static int szukajRecurencyjnie(int x,int y, int tab[]) {
        if (y>=tab.length){
            return -1;
        }
        if (x==tab[y]){
            return y;
        }else {
            return szukajRecurencyjnie(x,++y,tab);
        }
    }


}
