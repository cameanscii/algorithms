package pl.sda.algorytmy.stosArrayLista;

public class StosArrayList {

    private ListaArray<Object> listaArray;
    private int top = listaArray.getSize();
    private int maxSize = listaArray.size();

    public StosArrayList(ListaArray<Object> listaArray) {
        this.listaArray = listaArray;
    }

    public void push(Object o) {
        this.listaArray.add(o);
    }

    public Object pop() {
        return this.listaArray.get(top--);
    }

    public Object peek(){    return this.listaArray.get(top);
    }

    public boolean isEmpty(){return this.listaArray.isEmbpty();}
}




