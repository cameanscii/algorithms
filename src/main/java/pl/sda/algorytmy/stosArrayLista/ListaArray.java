package pl.sda.algorytmy.stosArrayLista;


    public class ListaArray <T>{

        private Object[] tab;
        private int size = 0;

        public Object[] getTab() {
            return tab;
        }

        public void setTab(Object[] tab) {
            this.tab = tab;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public ListaArray() {
            this.tab = new Object[5];
        }

        public ListaArray(int iloscElementow) {
            this.tab = new Object[iloscElementow];
        }

        public void add(Object object) {
            // jeśli jest za mało miejsca, rozszerz tablicę
            if (this.size >= this.tab.length) {
                rozszerzTablicę();
            }

            // dopisuje nowy element
            // wstawiamy do tablicy o indeksie [obecny_rozmiar]
            this.tab[size++] = object;
            // po
        }

        public void add(Object ... tablicaVarArgs) {
            for (Object object:tablicaVarArgs){
                add(object);
            }
        }

        @Override
        public String toString() {
            return "ListArray{" +
                    "tab=" + toStringTab() +
                    ", size=" + size +
                    '}';
        }

        private String toStringTab() {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < size; i++) {
                builder.append(tab[i] + ", ");
            }
            return builder.toString();
        }

        public void remove(int indeks){
            for (int i = indeks; i <this.size ; i++) {
                tab[i]=tab[i+1];
            }
            //redukujemy rozmiar tablicy
            this.size--;
            this.tab[this.size]=null;

        }

        private void rozszerzTablicę() {
            // brakuje nam miejsca w tablicy
            // kopiujemy elmenty z tablicy
            Object[] kopia = new Object[this.tab.length * 2];
            // przepisuje elementy ze starej tablicy do nowej
            for (int i = 0; i < this.tab.length; i++) {
                kopia[i] = this.tab[i];
            }

            // nadpisuję starą tablicę nową tablicą
            this.tab = kopia;
        }

        public int size() {
            return size;
            // nie jest odporne na wstawianie null'i w tablicę
//        for (int i = 0; i < tab.length; i++) {
//            if(tab[i] == null){
//                // jeśli znalazłem null, to znaczy że w tej komórce w tablicy jest pusto
//                return i;
//            }
//        }
//
//        return tab.length;
        }

        public Object get (int indeks){
            return this.tab[indeks];
        }

        // indexOf(Object o) - zwraca indeks elementu na którym występuje element o
        // contains (Object o) - zwraca true/false czy lista posiada dany obiekt
        // clear() - czyści tablicę z elementów

        public int indexOf(Object o){
            for (int i = 0; i < this.size; i++) {
                if (this.tab[i]==o){
                    return i;
                }
            }
            return -1;
        }

        public boolean contains(Object o){
            return indexOf(o)!=-1;
        }

        public void clear(Object o){
            for (int i = 0; i < this.size; i++) {
                if (this.tab[i]!=null){
                    this.tab[i]=0;
                }
            }
        }

        public boolean isEmbpty(){
            return this.size==0;
        }
        //do domu:
//
//    set(int indeks, Object o) - ustawia wartość na indeksie
//    remove(Object o) -- usuwa obiekt o z listy
//    isEmpty() - zwraca info czy lista jest pusta
//    removeAll(Object o) - usuwa wszystkie wystąpienia elementu z listy
//    lastIndexOf(Object o) - znajduje ostatni indeks elementu o w liście




    }




