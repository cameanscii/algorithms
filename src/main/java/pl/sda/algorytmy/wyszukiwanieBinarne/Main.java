package pl.sda.algorytmy.wyszukiwanieBinarne;

public class Main {
    public static void main(String[] args) {
        int[] tab = new int[]{-222, -56, 888, 999, 6000};
        int z= wyszukiwaniebinarne(-56,tab);
        z=binRec(0,tab.length-1,6000,tab);
        System.out.println(z);
    }


    public static int wyszukiwaniebinarne(int x, int[] tab) {

    int l =0;
    int r=tab.length -1;
    while (l<=r){
        int m=(l+r)/2;
        if (tab[m]<x){
            l=m+1;
        }else if(tab[m]>x){
            r=m-1;
        }else{
            return m;
        }
    }
    return -1;
    }

    public static int binRec(int l,int r, int x, int[] tab) {
        int m=(l+r)/2;

        if (tab[m]<x && l<=r ){
            return binRec(m+1,r, x, tab);
        }
        else if(tab[m]>x && l<=r){
            return binRec(l,m-1, x, tab);
        }
        else if (tab[m]==x && l<=r){
            return m;
        }

            return -1;
    }

}


